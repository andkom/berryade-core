<?php
/**
 * Entry point for the Berryade application. This file invokes the main handler for the apllication.
 *
 * @package Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 */

namespace Berryade\Core;

/** Load the berryade framework */
if (!($application = require 'ba-core/bootstrap.php') instanceof Application) {
    die('Cannot load the Berryade framework: '.$application);
}

try {
    /** Work out our routing for this page request */
    new Routing($application, $_SERVER['REQUEST_URI']);
} catch (\Exception $exception) {
    try {
        $translationSettings = new TranslationSettings();
        $error = Translator::translate(
            'cannot.load.application.route',
            null,
            $translationSettings,
            $application
        );
    } catch (\Exception $e) {
        // If we cannot translate for any reason
        $error = 'Cannot load application route';
    }

    $render = new Render($application);
    $render->exception(
        $exception,
        $error,
        $translationSettings
    );
    exit();
}
