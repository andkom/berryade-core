<?php
/**
 * Build items to bootstrap the site, autoload classes or that should only be ran once.
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Resource\Finder;

// Bootstrap our composer autoloader
if (($loader = realpath(__DIR__ . '/../../')) === false ||
    ! (@include $loader . '/autoload.php')) {
    throw new \Exception(
        'Could not initialise the autoloader'
    );
}

try {
    try {
        // Load our configuration file for this instance of the CMS
        $config = new Config(
            'berryade.yaml',
            Finder::path(Finder::PATH_ROOT),
            Config::LOADER_YAML,
            true,
            new Config\Validators\CoreRules()
        );
    } catch (Exception\FileNotFound $e) {
        $setupUrl = UrlGenerator::generate('setup', '/setup/');
        $setupUrlLen = strlen($setupUrl);

        // Instigate setup if required
        if (isset($_SERVER['REQUEST_URI']) &&
            (
                $setupUrlLen > strlen($_SERVER['REQUEST_URI']) ||
                substr($_SERVER['REQUEST_URI'], 0, $setupUrlLen) != $setupUrl
            )) {
            header('Location: '.$setupUrl, true, 307);
            exit();
        }

        $config = null;
    } catch (\Exception $exception) {
        throw $exception;
    }

    $application = new Application($config);
} catch (\Exception $exception) {
    $application = new Application();

    $render = new Render($application);
    $render->exception(
        $exception,
        'Cannot load the configuration file',
        new TranslationSettings()
    );

    exit();
}

return $application;
