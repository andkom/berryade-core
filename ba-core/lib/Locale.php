<?php
/**
 * Provides common methods for the locales
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

class Locale
{
    private $locale;

    /**
     * Save the locale we have loaded on instantiation
     * @return
     **/
    public function __construct(String $locale)
    {
        $this->setLocale($locale);
    }

    /**
     * Save the name of the locale that is loaded
     * @param String $locale - Name of the loaded locale
     * @return
     **/
    private function setLocale(String $locale)
    {
        $this->locale = $locale;
    }

    /**
     * Return the name of the locale that is loaded
     * @return String - Name of locale
     **/
    public function getLocale() : String
    {
        return $this->locale;
    }
}
