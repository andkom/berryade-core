<?php
/**
 * Provides a bridge between the twig service and the view system, allowing us to render the
 * templates to screen
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\Translator;
use \Berryade\Core\TranslationSettings;
use \Berryade\Core\Service;

class Render
{
    private $application;
    private $twigServices = [];

    /**
     * Application $application - Berryade application we will pass to the template
     * @return
     **/
    public function __construct(Application &$application)
    {
        $this->setApplication($application);
    }

    /**
     * Sets the application for use
     * @param Application $application - Berryade application
     * @return
     **/
    private function setApplication(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Fetches the application pointer for use
     * @return Application
     **/
    private function getApplication() : Application
    {
        return $this->application;
    }

    /**
     * Sets the twig service for use to a specific slot based on the locale of the translation
     * object
     * @param TranslationSettings $translationSettings - this sessions translation settings
     * @param Service $twigService - Twig service to store
     * @return
     **/
    private function setTwigService(
        TranslationSettings $translationSettings,
        Service $twigService = null
    ) {
        $key = $translationSettings->getKey();

        if (is_null($twigService)) {
            if (isset($this->twigServices[$key])) {
                unset($this->twigServices[$key]);
            }
            return;
        }

        $this->twigServices[$key] = $twigService;
    }

    /**
     * Returns the twig service for use from a specific slot based on the locale of the translation
     * object
     * @param TranslationSettings $translationSettings - this sessions translation settings
     * @return Service - Twig Service
     **/
    private function getTwigService(TranslationSettings $translationSettings)
    {
        $key = $translationSettings->getKey();

        if (!isset($this->twigServices[$key])) {
            $null = null;
            return $null;
        }

        return $this->twigServices[$key];
    }

    /**
     * Starts a twig service based on the locale of the translation
     * @param TranslationSettings $translationSettings - this sessions translation settings
     * @return Service - Twig Service
     **/
    public function startService(TranslationSettings $translationSettings)
    {
        $twigService = $this->getTwigService($translationSettings);

        if (!is_null($twigService)) {
            return $twigService;
        }

        $application = $this->getApplication();

        $this->setTwigService(
            $translationSettings,
            $application->service->start(
                Service::SERVICE_TWIG,
                [
                    'locale'    =>  $translationSettings->getLocale()
                ]
            )
        );

        return $this->getTwigService($translationSettings);
    }

    /**
     * Pauses a twig service based on the locale of the translation
     * @param TranslationSettings $translationSettings - this sessions translation settings
     * @return
     **/
    public function pauseService(TranslationSettings $translationSettings)
    {
        $twigService = $this->getTwigService($translationSettings);
        if (is_null($twigService)) {
            return;
        }

        $application = $this->getApplication();
        $application->service->end($twigService);
        $this->setTwigService($translationSettings, null);
    }

    /**
     * Tells the twig service we wish to use a form and to provide a form builder
     * @param TranslationSettings $translationSettings - this sessions translation settings
     * @return FormFactory
     **/
    public function formBuilder(TranslationSettings $translationSettings)
    {
        $twigService = $this->getTwigService($translationSettings);
        if (is_null($twigService)) {
            return;
        }

        return $twigService->interface->formBuilder();
    }
    
    /**
     * Loads the relevant templates for the resource and stores the data for rendering
     * @param String $resource - Name of the view to load
     * @param Array $paramaters - Data to pass through to the template
     * @param TranslationSettings translationSettings - any translation settings for the content
     * @return
     **/
    public function view(
        string $resource,
        array $paramaters = null,
        TranslationSettings $translationSettings
    ) {
        try {
            $twig = $this->startService($translationSettings);
            $twig->interface->render(
                $translationSettings,
                $resource,
                $paramaters
            );
            
            $this->pauseService($translationSettings);
        } catch (\Exception $exception) {
            $application = $this->getApplication();

            echo '<p>' . Translator::translate(
                'render.error',
                [
                    '%template%' => $resource
                ],
                $translationSettings,
                $application
            ) . '</p>';

            $exceptions = $this->buildExceptions($exception);
            foreach ($exceptions as $exception) {
                echo '<p>'.$exception['message'].'</p>';
            }
            exit();
        }
    }

    /**
     * Renders a fatal exception and then loads the correct view
     * @param Exception $exception containing a single or chain of exceptions
     * @param String $safeMessage - A user safe message to show
     * @param TranslationSettings translationSettings - any translation settings for the content
     * @return
     **/
    public function exception(
        \Exception $exception,
        string $safeMessage,
        TranslationSettings $translationSettings
    ) {
        $this->view(
            'error/fatal',
            [
                'message'   =>  $safeMessage,
                'errors'    =>  $this->buildExceptions($exception)
            ],
            $translationSettings
        );
    }

    /**
     * Builds a list of exeption data for the template from the exception chain
     * @param Exception $exception containing a single or chain of exceptions
     * @return Array of exception chain data
     **/
    private function buildExceptions(\Exception $exception) : array
    {
        $exceptions = array();

        $application = $this->getApplication();
        $testMode = ($application->getConfig('test') !== false ? true : false);

        do {
            $logMessage = Translator::translate(
                'caught.exception',
                [
                    '%number%'  => $exception->getCode(),
                    '%line%'    => $exception->getLine(),
                    '%message%' => $exception->getMessage(),
                    '%file%'    => $exception->getFile()
                ],
                new TranslationSettings(
                    $application->getConfig('locale')
                ),
                $application
            );

            \Berryade\Core\Log::entry(
                $application,
                __NAMESPACE__,
                'yPEtYYBHfsDmPU',
                $logMessage,
                \Berryade\Core\Log::ERROR,
                [
                    'number'    => $exception->getCode(),
                    'line'      => $exception->getLine(),
                    'message'   => $exception->getMessage(),
                    'file'      => $exception->getFile(),
                ]
            );

            if (!$testMode &&
                !$exception instanceof \Berryade\Core\Exception\UserSafe) {
                continue;
            }

            $exceptions[] = [
                'number'    =>  $exception->getCode(),
                'line'      =>  $exception->getLine(),
                'message'   =>  $exception->getMessage()
            ];
        } while ($exception = $exception->getPrevious());

        return $exceptions;
    }
}
