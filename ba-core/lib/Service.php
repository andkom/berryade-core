<?php
/**
 * Service wrapper - provides a container for all services and common management methods
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Services;
use \Berryade\Core\Translator;
use \Berryade\Core\TranslationSettings;

class Service
{
    const SERVICE_TWIG = 'Twig';
    const SERVICE_TRANSLATOR = 'Translator';
    const SERVICE_LOG = 'Log';

    // A link to the parent service container
    private $serviceContainer;
    // The type of service this is (one of the SERVICE_X constant values)
    private $serviceType;
    // Any configuation paramaters to pass to the service
    private $serviceConfig;
    // The service id - this is based on the type and configuration provided
    private $serviceId;
    // The channel - this is where in the controller we are
    private $serviceChannel;
    // The interface - where the magic happens
    public $interface;

    /**
     * Instantiate the service
     * @param Services $container - The container for all of the services
     * @param String $serviceType - Tne type of the service
     * @param Array $configurationParams - Configuration paramaters
     * @return
     **/
    public function __construct(
        Services &$container,
        String $serviceType,
        array $configurationParams = null
    ) {
        $this->setServiceContainer($container);
        $this->setServiceType($serviceType);
        $this->setServiceConfiguration($configurationParams);
        $this->setServiceId();
        $this->setInterface();
    }


    /**
     * Checks the service type is valid
     * @param String $serviceType
     * @return Bool
     **/
    private function validServiceType(String $serviceType) : Bool
    {

        $application = $this->getServiceContainer()->getApplication();

        switch ($serviceType) {
            case self::SERVICE_TWIG:
            case self::SERVICE_TRANSLATOR:
            case self::SERVICE_LOG:
                break;
            default:
                throw new \Berryade\Core\Exception\LogicException(
                    Translator::translate(
                        'service.type.error',
                        [ '%service%' => $serviceType ],
                        new TranslationSettings(),
                        $application
                    )
                );
        }

        return true;
    }

    /**
     * Sets the service type of this service
     * @param String - Service type
     **/
    private function setServiceType(String $serviceType)
    {
        $this->validServiceType($serviceType);
        $this->serviceType = $serviceType;
    }

    /**
     * Returns the service type
     * @return String
     **/
    public function getServiceType() : String
    {
        return $this->serviceType;
    }

    /**
     * Sets any service configuration or null if no configuration exists
     * @param Array $configurationParams - Configuration paramaters
     * @return
     **/
    private function setServiceConfiguration(array $configurationParams = null)
    {
        $this->serviceConfig = $configurationParams;
    }

    /**
     * Returns the service configuration or null if no configuration exists
     * @return Mixed
     **/
    public function getServiceConfiguration()
    {
        return $this->serviceConfig;
    }

    /**
     * Returns the parent service container
     * @return Services - parent service container
     **/
    private function &getServiceContainer() : Services
    {
        return $this->serviceContainer;
    }

    /**
     * Sets the parent service container
     * @param Services - parent service container
     **/
    private function setServiceContainer(Services &$container)
    {
        $this->serviceContainer = &$container;
    }

    /**
     * Sets the service identifier for this service
     * @return
     **/
    private function setServiceId()
    {
        $serviceType = $this->getServiceType();
        $serviceConfig = $this->getServiceConfiguration();

        if (is_null($serviceConfig)) {
            $this->serviceId = $serviceType . '_default';
            return;
        }

        $this->serviceId = $serviceType.'_'.md5(serialize($serviceConfig));
        return;
    }

    /**
     * Returns the service identifier for this service
     * @return String - service identifier
     **/
    public function getServiceId() : String
    {
        return $this->serviceId;
    }

    /**
     * Sets the interface handler for this service
     * @return
     **/
    public function setInterface()
    {
        $serviceClass = '\\Berryade\\Core\\Service\\Service'.$this->getServiceType();
        $this->interface = new $serviceClass(
            $this->getServiceContainer(),
            $this->getServiceConfiguration()
        );
    }

    /**
     * Sets the channel identifier for this service
     * @param Int $channel
     **/
    public function setChannel(Int $channel)
    {
        $this->serviceChannel = $channel;
    }

    /**
     * Returns the channel identifier for this service
     * @return Int channel
     **/
    public function getChannel() : Int
    {
        return $this->serviceChannel;
    }
}
