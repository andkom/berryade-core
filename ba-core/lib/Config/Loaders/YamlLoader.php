<?php
/**
 * Provides a mechanism to load a Yaml file
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Config\Loaders;

use \Symfony\Component\Config\Loader\FileLoader;
use \Symfony\Component\Yaml\Yaml;

class YamlLoader extends FileLoader
{
    /**
     * Load and parse a Yaml file returning its configuration settings
     * @param String $resource - The resource location
     * @param String/null $type - The resource type or null if unknown
     * @return Array
    **/
    public function load($resource, $type = null) : Array
    {
        try {
            $configValues = Yaml::parse(file_get_contents($resource));
        } catch (\Berryade\Core\Exception $exception) {
            throw $exception;
        } catch (\Exception $exception) {
            throw new \Berryade\Core\Exception\ParseError(
                sprintf(
                    'Could not parse file \'%s\'',
                    $resource
                ),
                null,
                $exception
            );
        }

        if (is_null($configValues)) {
            $configValues = [];
        }

        return $configValues;
    }

    /**
     * Returns true if the supplied resource type is compatible with our loader
     * @param String $resource - The resource location
     * @param String/null $type - The resource type or null if unknown
     * @return Bool
     **/
    public function supports($resource, $type = null) : Bool
    {
        return is_string($resource) && 'yaml' === pathinfo(
            $resource,
            PATHINFO_EXTENSION
        );
    }
}
