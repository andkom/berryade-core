<?php
/**
 * Validation rules for use when loading the berryade.yaml file
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Config\Validators;
 
use \Berryade\Core\Locales;
use \Symfony\Component\Config\Definition\ConfigurationInterface;
use \Symfony\Component\Config\Definition\Builder\TreeBuilder;

class CoreRules implements ConfigurationInterface
{
    /**
     * Validation rules for the ba-core.yaml config file
     * @return TreeBuilder() object or an exception on error
     **/
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('berryade');

        $rootNode
            ->children()
                ->booleanNode('test')
                    ->info(
                        'If the system should be ran in TEST mode, ' .
                        'showing a higher level of verbosity.'
                    )
                    ->defaultFalse()
                ->end()
                ->variableNode('locale')
                    ->info('The locale for this site.')
                    ->defaultValue('en_GB')
                    ->validate()
                        ->ifTrue(function ($value) {
                            $locales = new Locales();
                            return !$locales->supported($value);
                        })
                        ->thenInvalid('Unsupported locale: "%s"')
                    ->end()
                ->end()
                ->arrayNode('log')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->integerNode('history')
                            ->info(
                                'Number of days to retain logs for'
                            )
                            ->min(1)
                            ->max(365)
                            ->defaultValue(7)
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('mysql')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->integerNode('port')
                            ->info('MySQL port number')
                            ->min(1)
                            ->max(65535)
                            ->defaultValue(3306)
                        ->end()
                        ->variableNode('host')
                            ->info('MySQL hostname')
                            ->defaultValue('localhost')
                        ->end()
                        ->variableNode('user')
                            ->info('MySQL username')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->variableNode('pass')
                            ->info('MySQL password')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
        return $treeBuilder;
    }

    /**
     * Converts the data into usable objects
     * @param array $configData - Loaded configuration data
     * @return array - converted data
     **/
    public function convertData(array $configData): array
    {
        $locales = new Locales();
        $configData['locale'] = $locales->load($configData['locale']);

        return $configData;
    }
}
