<?php
/**
 * Provides a bridge between the application and the log service making it quicker to push a log
 * message to the log files
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\Translator;
use \Berryade\Core\TranslationSettings;

class Log
{
    const DEBUG      =   100;
    const INFO       =   200;
    const NOTICE     =   250;
    const WARNING    =   300;
    const ERROR      =   400;
    const CRITICAL   =   500;
    const ALERT      =   550;
    const EMERGENCY  =   600;

    /**
     * Push a log entry into the log system
     * @param Application - Berryade application
     * @param string namespace - The namespace this message originated from
     * @param string msgIdentifier - Unique identifier for this message to aid tracing origin
     * @param string msg - Body of the message
     * @param int level - The log level to use
     * @param array data - Any data you wish to pass through to the log
     * @return
     **/
    public static function entry(
        Application $application,
        string $namespace,
        string $msgIdentifier,
        string $msg,
        int $level = null,
        array $data = null
    ) {

        $logger = $application->service->start(
            Service::SERVICE_LOG,
            [
                'identifier' => self::namespaceToIdent($namespace)
            ]
        );
        
        $logger->interface->log($msgIdentifier, $msg, $level, $data);
        $application->service->end($logger);
    }

    /**
     * Converts the namespace into a log section identifier
     * @param string namespace - Namespace of caller
     * @return string log section identifier
     **/
    private static function namespaceToIdent(string $namespace): string
    {
        return str_replace('\\', '_', $namespace);
    }
}
