<?php
/**
 * Groups translation settings together for the translator
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Locale;
use \Berryade\Core\Locales;

class TranslationSettings
{
    private $section;
    private $module;
    private $locale;

    /**
     * Store our translation options
     * @param Locale locale - The locale to use when performing the translation
     * @param String section - The name of the file the translation resides in
     * @param String module - The name of the module responsible for the translation
     * @return
     **/
    public function __construct(
        Locale $locale = null,
        String $section = 'berryade',
        String $module = 'core'
    ) {
        $this->setSection($section);
        $this->setModule($module);
        $this->setLocale($locale);
    }

    /**
     * Stores the section
     * @param String section
     * @return
     **/
    private function setSection(String $section)
    {
        $this->section = $section;
    }

    /**
     * Returns the section
     * @return String section
     **/
    public function getSection() : String
    {
        return $this->section;
    }

    /**
     * Stores the module
     * @param String module
     * @return
     **/
    private function setModule(String $module)
    {
        $this->module = $module;
    }

    /**
     * Returns the module
     * @return String module
     **/
    public function getModule() : String
    {
        return $this->module;
    }

    /**
     * Stores the locale
     * @param Locale locale
     * @return
     **/
    private function setLocale(Locale $locale = null)
    {
        if (is_null($locale)) {
            $locales = new Locales();
            $locale = $locales->load('en_gb');
        }

        $this->locale = $locale;
    }

    /**
     * Returns the locale
     * @return Locale locale
     **/
    public function getLocale() : Locale
    {
        return $this->locale;
    }

    /**
     * Returns the locale name
     * @return String locale name
     **/
    public function getLocaleName() : String
    {
        return $this->locale->getLocale();
    }

    /**
     * Returns a translation unique key
     * @return String unique key
     **/
    public function getKey() : String
    {
        return $this->getLocaleName() . '_' . $this->getModule() . '_' . $this->getSection();
    }
}
