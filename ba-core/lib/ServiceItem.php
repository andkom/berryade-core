<?php
/**
 * Provides the services with a mechanism to interact with the application and service controller
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\Data;
use \Berryade\Core\Services;

class ServiceItem
{
    // Parent services container
    private $serviceContainer;

    // Service configuration
    private $serviceConfiguration;

    /**
     * Save a reference to the service container
     * @return
     **/
    public function __construct(Services $serviceContainer, array $serviceConfiguration = null)
    {
        $this->setServiceContainer($serviceContainer);
        $this->setServiceConfiguration($serviceConfiguration);
    }

    /**
     * Sets the service container reference for use by the services
     * @param Services $serviceContainer - the container for all of the services
     * @return
     **/
    private function setServiceContainer(Services $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * Returns the service container for use by the services
     * @return Services - service container
     **/
    public function getServiceContainer() : Services
    {
        return $this->serviceContainer;
    }

    /**
     * Sets the service configuration
     * @param Array $serviceConfiguration
     * @return
     **/
    private function setServiceConfiguration(array $serviceConfiguration = null)
    {
        if (is_null($serviceConfiguration)) {
            $serviceConfiguration = [];
        }

        $this->serviceConfiguration = $serviceConfiguration;
    }

    /**
     * Returns the service configuration or a subset of the information if a key is supplied
     * @param string/array/null $key - Key to return data for, this could be nested in an array for
     * a deeper path, a string for bottom level or null for the whole array
     * @param Bool suppressError - If true then return null if the item is not set instead of
     * throwing an out of bounds error
     * @return Mixed - Service configuration item
     **/
    public function getServiceConfiguration($key = null, Bool $supressError = false)
    {
        return Data::getNestedValue(
            $this->getServiceContainer()->getApplication(),
            $this->serviceConfiguration,
            $key,
            $supressError
        );
    }

    /**
     * Returns the application for use by the services
     * @return Application - Berryade application
     **/
    public function getApplication() : Application
    {
        return $this->getServiceContainer()->getApplication();
    }
}
