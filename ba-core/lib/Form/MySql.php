<?php
/**
 * MySQL - fields and validation
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class MySql extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'host',
                TextType::class,
                [
                    'label'         => 'Host',
                    'data'          => 'localhost',
                    'constraints'   => new NotBlank()
                ]
            )
            ->add(
                'port',
                IntegerType::class,
                [
                    'label'         =>  'Port',
                    'data'          =>  3306,
                    'constraints'   =>  new Range(
                        [
                            'min'   =>  1,
                            'max'   =>  65535
                        ]
                    ),
                    'attr'          => ['min' => 1, 'max' => 65535 ]
                ]
            )
            ->add(
                'user',
                TextType::class,
                [
                    'label'         => 'User',
                    'constraints'   => new NotBlank()
                ]
            )
            ->add(
                'pass',
                PasswordType::class,
                [
                    'label'         => 'Password',
                    'constraints'   => new NotBlank()
                ]
            )
        ;
    }
}
