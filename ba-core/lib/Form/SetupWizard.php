<?php
/**
 * Setup - Configuration Wizard Form - fields and validation
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Form;

use Berryade\Core\Form\MySql;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class SetupWizard extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'test',
                ChoiceType::class,
                [
                    'expanded'          => true,
                    'choices'           => [
                        'Yes'           => true,
                        'No'            => false
                    ],
                    'data'              => false
                ]
            )
            ->add(
                'locale',
                ChoiceType::class,
                [
                    'placeholder'       => 'Please select a locale...',
                    'constraints'       => new NotBlank(),
                    'choices'           => [
                        'English (United Kingdom)' => 'en_GB',
                    ],
                ]
            )
            ->add(
                'mysql',
                MySql::class
            )
            ->add('save', SubmitType::class)
            ->add('cancel', SubmitType::class)
        ;
    }
}
