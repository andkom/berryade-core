<?php
/**
 * User Safe  - A user safe exception - one that is safe to serve up to the end user
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Exception;

class UserSafe extends \Exception implements \Berryade\Core\Exception
{
}
