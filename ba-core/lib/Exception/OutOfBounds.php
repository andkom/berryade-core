<?php
/**
 * Out of Bounds - Thrown if an argument is an invalid key or not between an expected range
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Exception;

class OutOfBounds extends \OutOfBoundsException implements \Berryade\Core\Exception
{
}
