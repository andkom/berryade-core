<?php
/**
 * Invalid Argument - Thrown if an argument is not of the expected type
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Exception;

class InvalidArgument extends \InvalidArgumentException implements \Berryade\Core\Exception
{
}
