<?php
/**
 * Runtime - Exception thrown if an error which can only be found on runtime occurs.
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Exception;

class RunTime extends \RuntimeException implements \Berryade\Core\Exception
{
}
