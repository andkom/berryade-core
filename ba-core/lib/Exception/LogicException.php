<?php
/**
 * Logic Exception - Thrown if the developers logic is of course
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Exception;

class LogicException extends \LogicException implements \Berryade\Core\Exception
{
}
