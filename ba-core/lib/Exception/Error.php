<?php
/**
 * An error that has been turned into an exception
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Exception;

class Error extends Runtime implements \Berryade\Core\Exception
{
}
