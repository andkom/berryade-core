<?php
/**
 * Provides a quick call mechanism to use the Translator method, recycling the stream after
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\TranslationSettings;

class Translator
{
    private $application;

    /**
     * Sets the application for use
     * @param Application $application - Berryade application
     * @return
     **/
    private function setApplication(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Fetches the application pointer for use
     * @return Application
     **/
    private function &getApplication() : Application
    {
        return $this->application;
    }

    /**
     * Translates a value
     * @param String $key - The translation identifier
     * @param Array $data - The translation placeholder data
     * @param TranslationSettings translationSettings - The settings we should use for translating
     * @param Application $application - Berryade application
     * any content
     * @return String - Translation
     **/
    public static function translate(
        string $key,
        array $data = null,
        TranslationSettings $translationSettings,
        Application $application
    ) : string {

        $translator = $application->service->start(
            Service::SERVICE_TRANSLATOR,
            [
                'locale' => $translationSettings->getLocale()
            ]
        );

        $translation = $translator->interface->translate($key, $data, $translationSettings);

        $application->service->end($translator);
        return $translation;
    }
}
