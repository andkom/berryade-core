<?php
/**
 * Manages the Berryade modules
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\ModuleInfo;

use \Berryade\Resource\Finder;

class Modules
{
    /**
     * Lists the modules installed in the system
     * TODO
     * @param bool state - true = active only, false = disabled only, null - either
     **/
    public static function list(Application $application, bool $state = null) : array
    {
        //TODO state
        //TODO cache
        $matches = glob(Finder::path(Finder::PATH_MODULE) . '*');
        $modules = array();

        foreach ($matches as $modulePath) {
            try {
                $modules[] = new ModuleInfo($application, $modulePath);
            } catch (\Throwable $throwable) {
                var_dump($throwable->getMessage(), $throwable->getFile(), $throwable->getLine());
            } catch (\Error $error) {
                echo 456;
            } catch (\Exception $exception) {
                echo $exception->getMessage();
                continue;
            }
        }

        return $modules;
    }
}

// name
// info
// shortname
// path
// version
// author
// vendor
// link
// core version - min - max
// dependencies - min - max
// optional dependencies
// installed
// status
// settings
// activate / deactivate
// documentation
// faqs


// status
//   -- are dependencies installed
//   -- are dependencies healthy
//   -- are required settings filled out

// saving settings will clear the module status cache
// deactivating the module will clear the status of this module and any dependents
// activating the module will clear the status of this module and any dependents
