<?php
/**
 * Provides a mechanism to write to the log system
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Service;

use \Berryade\Core\ServiceInterface;
use \Berryade\Core\ServiceItem;
use \Berryade\Core\Translator;
use \Berryade\Core\TranslationSettings;

use \Berryade\Resource\Finder;

use \Monolog\Logger;
use \Monolog\Handler\BrowserConsoleHandler;
use \Monolog\Handler\RotatingFileHandler;

class ServiceLog extends ServiceItem implements ServiceInterface
{
    // Holds our monolog logger
    private $logger;

    // Holds the message unique identifier
    private $msgIdentifier;

    // Holds the transactional and session information to trace requests
    private $traceInfo;

    /**
     * Required function to contain any code when the service is paused (i.e. available)
     **/
    public function pause()
    {
    }

    /**
     * Required function to contain any code when the service is resumed (i.e. recycled)
     **/
    public function resume()
    {
    }

    /**
     * Required function to contain any code when the service is instantiated (i.e. created)
     **/
    public function start()
    {
        $this->loadLogger($this->getServiceConfiguration('identifier', false));
    }

    /**
     * Required function to contain any code when the service is ended (i.e. destroyed)
     **/
    public function end()
    {
    }

    /**
     * Loads the monolog logger and registers our handlers and additional information injectors
     * @param string identifier - the log stream identifier
     * @return
     **/
    private function loadLogger(string $identifier)
    {
        $this->setTraceInfo();

        $logger = new Logger($identifier);

        //$logger->pushProcessor([ $this, 'injectUserInfo' ]); //TODO add user information
        $logger->pushProcessor([ $this, 'injectTraceInfo' ]);
        $logger->pushProcessor([ $this, 'injectMsgIdentInfo' ]);


        $this->setLoggger(
            new Logger($identifier)
        );

        // log to disk
        $this->useHandlerStream();

        // log to browser
        $this->useHandlerBrowserConsole();
    }

    /**
     * Stores the transactional and session information to trace requests
     * @return
     **/
    private function setTraceInfo()
    {
        $this->traceInfo = [
            'session'       =>  session_id(),
            'transaction'   =>  uniqid('BAL')
        ];
    }

    /**
     * Returns the transactional and session information
     * @return array trace information
     **/
    private function getTraceInfo() : array
    {
        return $this->traceInfo;
    }

    /**
     * Stores the monolog logger
     * @param Logger logger - monolog logger
     * @return
     **/
    private function setLoggger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Returns the monolog logger
     * @return Logger - monolog logger
     **/
    private function getLogger() : Logger
    {
        return $this->logger;
    }

    /**
     * Adds the trace information into the message record
     * @param array $record - message record
     * @return array $record - amended message record
     **/
    public function injectTraceInfo(array $record) : array
    {
        $config = $this->getTraceInfo();

        $record['extra']['session'] = $config['session'];
        $record['extra']['transaction'] = $config['transaction'];
        return $record;
    }

    /**
     * Adds the message identifier into the message record
     * @param array $record - message record
     * @return array $record - amended message record
     **/
    public function injectMsgIdentInfo(array $record) : array
    {
        $record['extra']['message'] = $this->getMessageIdentifier();
        return $record;
    }

    /**
     * Logs the message to disk
     * @return
     **/
    private function useHandlerStream()
    {
        $application = $this->getApplication();

        $log_level = ($application->getConfig('test') !== false
            ? \Monolog\Logger::DEBUG
            : \Monolog\Logger::ERROR
        );

        $max_files = $application->getConfig([ 'log', 'history' ]);

        $this->getLogger()->pushHandler(
            new RotatingFileHandler(
                Finder::path(Finder::PATH_LOGS) . 'berryade',
                $max_files,
                $log_level
            )
        );
    }

    /**
     * Logs the message to the browser console if in TEST mode
     * @return
     **/
    private function useHandlerBrowserConsole()
    {
        if ($this->getApplication()->getConfig('test') === false) {
            return;
        }

        $this->getLogger()->pushHandler(
            new BrowserConsoleHandler(Logger::DEBUG)
        );
    }

    /**
     * Returns the correct function to use for the specified log level
     * @param int level - Log level
     * @return
     **/
    private function getLoggerFunction(int $level) : string
    {
        switch ($level) {
            case Logger::DEBUG:
                return 'debug';
            case Logger::INFO:
                return 'info';
            case Logger::NOTICE:
                return 'notice';
            case Logger::WARNING:
                return 'warning';
            case Logger::ERROR:
                return 'error';
            case Logger::CRITICAL:
                return 'critical';
            case Logger::ALERT:
                return 'alert';
            case Logger::EMERGENCY:
                return 'notice';
        }

        $application = $this->getApplication();

        throw new \Berryade\Core\Exception\LogicException(
            Translator::translate(
                'log.level.invalid',
                [
                    '%level%'   =>  $level
                ],
                new TranslationSettings(
                    $application->getConfig('locale')
                ),
                $application
            )
        );
    }

    /**
     * Stores the message unique identifier used to trace the origin of the message
     * @param string msgIdentifier - Message unique identifier
     * @return
     **/
    private function setMessageIdentifier(string $msgIdentifier)
    {
        $this->msgIdentifier = $msgIdentifier;
    }

    /**
     * Returns the message unique identifier used to trace the origin of the message
     * @return string - Message unique identifier
     **/
    private function getMessageIdentifier() : string
    {
        return $this->msgIdentifier;
    }

    /**
     * Logs the message to the system
     * @param string msgIdentifier - Unique identifier for this message to aid tracing origin
     * @param string msg - Body of the message
     * @param int level - The log level to use
     * @param array data - Any data you wish to pass through to the log
     * @return
     **/
    public function log(string $msgIdentifier, string $msg, int $level = null, array $data = null)
    {
        if (is_null($level)) {
            $log_level = Logger::INFO;
        }

        if (is_null($data)) {
            $data = [];
        }

        $this->setMessageIdentifier($msgIdentifier);

        $loggerFunction = $this->getLoggerFunction($level);

        $this->getLogger()->$loggerFunction($msg, $data);
    }
}
