<?php
/**
 * Translation Service - Bridge between Berryade and the Translation system
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Service;

use \Berryade\Core\Data;
use \Berryade\Core\Locale;
use \Berryade\Core\ServiceInterface;
use \Berryade\Core\ServiceItem;
use \Berryade\Core\Translator as TextTranslator;
use \Berryade\Core\TranslationSettings;

use \Berryade\Resource\Finder;

use \Symfony\Component\Translation\Translator;
use \Symfony\Component\Translation\Loader\YamlFileLoader;

class ServiceTranslator extends ServiceItem implements ServiceInterface
{
    private $loadedLibraries;
    private $locale;
    private $transfer;

    /**
     * Required function to contain any code when the service is paused (i.e. available)
     **/
    public function pause()
    {
    }

    /**
     * Required function to contain any code when the service is resumed (i.e. recycled)
     **/
    public function resume()
    {
    }

    /**
     * Required function to contain any code when the service is instantiated (i.e. created)
     **/
    public function start()
    {
        $this->setLocale();
        $this->startTranslator();
    }

    /**
     * Required function to contain any code when the service is ended (i.e. destroyed)
     **/
    public function end()
    {
    }

    /**
     * Sets the locale for this translation service stream
     **/
    private function setLocale()
    {
        if (!(($locale = $this->getServiceConfiguration('locale', true)) instanceof Locale)) {
            throw new \Berryade\Core\Exception\LogicException(
                'You need to supply a locale to perform a translation'
            );
        }

        $this->locale = $locale;
    }

    /**
     * Returns the locale for this translation service stream
     * @return Locale - Locale
     **/
    private function getLocale() : Locale
    {
        return $this->locale;
    }

    /**
     * Returns the name locale for this translation service stream
     * @return String - Locale name
     **/
    private function getLocaleName() : string
    {
        return $this->locale->getLocale();
    }

    /**
     * Setup the translator service
     * @return
     **/
    private function startTranslator()
    {
        // Only cache when not in test mode
        $caheFile = ($this->getApplication()->getConfig('test', true) === false
            ? null
            : Finder::path(Finder::PATH_CACHE, true) . 'translations'
        );

        $this->translator = new Translator(
            $this->getLocaleName(),
            null,
            $caheFile
        );

        // Reset our loaded libraries
        $this->loadedLibraries = [];

        // Add our Yaml loader
        $this->translator->addLoader('yaml', new YamlFileLoader());

        // Add our fallback locales
        $this->translator->setFallbackLocales($this->fallbackLocales());
    }

    /**
     * Translates a string into the locale for this stream
     * @param String $key - The translation identifier
     * @param Array $data - The translation placeholder data
     * @param TranslationSettings translationSettings - settings determining the translation
     * @return String - Translation
     **/
    public function translate(
        string $key,
        array $data = null,
        TranslationSettings $translationSettings
    ) : string {
        $this->loadLibraries($translationSettings);

        if (is_null($data)) {
            $data = [];
        }

        // if we could not get a translation - change the key into something more useful
        if ($key == ($translation = $this->translator->trans($key, $data))) {
            $translation = str_replace('.', ' ', $translation);
        }

        return $translation;
    }

    /**
     * Returns a list of locales linked to our current along with an english one to fallback to
     * @return Array - An array of locales
     **/
    private function getLocales() : array
    {
        $currentLocale = $this->getLocaleName();

        // Add the seed locale as mandatory
        $locales = [ $currentLocale => true ];

        // Add lanuage general as optional
        if (($pos = strpos($currentLocale, '_'))!== false) {
            $locales[substr($currentLocale, 0, $pos)] = false;
        }

        // Add english as optional
        if (!isset($locales['en'])) {
            $locales['en'] = false;
        }

        return $locales;
    }

    /**
     * Returns a list of locales to fallback to, essentially the getLocales() without the current
     * @return Array - Array of locales to fallback to
     **/
    private function fallbackLocales() : array
    {
        $currentLocale = $this->getLocaleName();
        $locales = $this->getLocales();
        unset($locales[$currentLocale]);
        return array_keys($locales);
    }

    /**
     * Loads the relevant translation files from the correct location
     * @param TranslationSettings translationSettings - settings determining the translation
     * @return
     **/
    public function loadLibraries(TranslationSettings $translationSettings)
    {
        if ($this->isLibraryLoaded($translationSettings)) {
            return;
        }

        $this->setLibraryLoaded($translationSettings);
       
        $locales = $this->getLocales($this->getLocale());

        $section = $translationSettings->getSection();
        $module = $translationSettings->getModule();

        foreach ($locales as $locale => $mandatory) {
            $translationFile = $this->getFile($section, $module, $locale);

            if (file_exists($translationFile)) {
                $this->translator->addResource(
                    'yaml',
                    $translationFile,
                    $locale
                );
            }
        }
    }

    /**
     * Returns if this module / section combination has already been loaded previously or not
     * @param TranslationSettings translationSettings - settings determining the translation
     * @return Bool - True if already loaded
     **/
    private function isLibraryLoaded(TranslationSettings $translationSettings) : bool
    {
        return isset($this->loadedLibraries[$translationSettings->getKey()]);
    }

    /**
     * Marks the fact this library has been loaded
     * @param TranslationSettings translationSettings - settings determining the translation
     * @return
     **/
    private function setLibraryLoaded(TranslationSettings $translationSettings)
    {
        $this->loadedLibraries[$translationSettings->getKey()] = true;
    }

    /**
     * Returns a pointer to the symfony translator for inclusion in service bridges
     **/
    public function &getTranslator() : Translator
    {
        return $this->translator;
    }

    /**
     * Returns the path and name of the translation file containing the translation text
     * @param String section - The name of the translation file the translation resides in
     * @param String module - The name of the module responsible for the translation file
     * @param String localeName - The name of the locale the file is in
     * @return String - Path and name of translation file
     **/
    public function getFile(string $section, string $module, string $localeName) : string
    {
        $basePath = ($module == 'core'
            ? Finder::path(Finder::PATH_TRANSLATIONS)
            : Finder::path(Finder::PATH_MODULE_TRANSLATIONS) . $module . '/translations/'
        );

        $fileName = $section . '_' . $localeName . '.yaml';

        return $basePath . $fileName;
    }
}
