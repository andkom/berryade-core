<?php
/**
 * Twig Service - Bridge between Berryade and the Twig system
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Service;

use \Berryade\Core\Service;
use \Berryade\Core\ServiceInterface;
use \Berryade\Core\ServiceItem;
use \Berryade\Core\TranslationSettings;
use \Berryade\Core\UrlGenerator;

use \Berryade\Resource\Finder;

use \Symfony\Bridge\Twig\Extension\FormExtension;
use \Symfony\Bridge\Twig\Extension\TranslationExtension;
use \Symfony\Bridge\Twig\Form\TwigRenderer;
use \Symfony\Bridge\Twig\Form\TwigRendererEngine;
use \Symfony\Component\Form\Extension\Csrf\CsrfExtension;
use \Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use \Symfony\Component\Form\Forms;
use \Symfony\Component\Form\FormFactory;
use \Symfony\Component\Form\FormFactoryBuilder;
use \Symfony\Component\Form\FormView;
use \Symfony\Component\Security\Csrf\CsrfTokenManager;
use \Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use \Symfony\Component\Security\Csrf\TokenStorage\NativeSessionTokenStorage;
use \Symfony\Component\Translation\Translator;
use \Symfony\Component\Translation\Loader\XliffFileLoader;
use \Symfony\Component\Validator\Validation;

class ServiceTwig extends ServiceItem implements ServiceInterface
{
    // Twig instance
    private $twig;

    // Translator instance
    private $translator;

    // Form Loaded
    private $formFactory;

    /**
     * Required function to contain any code when the service is paused (i.e. available)
     **/
    public function pause()
    {
        $application = $this->getApplication();
        $application->service->end($this->translator);
    }

    /**
     * Required function to contain any code when the service is resumed (i.e. recycled)
     **/
    public function resume()
    {
        if ($this->openTranslator() === false) {
            return;
        }
    }

    /**
     * Required function to contain any code when the service is instantiated (i.e. created)
     **/
    public function start()
    {
        $this->prepareEnvironment();
    }

    /**
     * Required function to contain any code when the service is ended (i.e. destroyed)
     **/
    public function end()
    {
    }

    /**
     * Sets up the twig environment for use
     * @return
     **/
    private function prepareEnvironment()
    {
        $twigOptions = array();

        $application = $this->getApplication();

        // If we are not running in test mode then we should cache
        if ($application->getConfig('test') !== false) {
            $twigOptions['cache'] = Finder::path(Finder::PATH_TEMPLATES_CACHE);
        }

        // Load the environment
        $loader = new \Twig_Loader_Filesystem([
            Finder::path(Finder::PATH_TEMPLATES),
            Finder::path(Finder::PATH_VENDOR) . 'symfony/twig-bridge/Resources/views/Form'
        ]);
        $this->setTwig(new \Twig_Environment(
            $loader,
            $twigOptions
        ));

        $this->loadTranslator();
    }

    /**
     * Loads the translator and all of the relevant locale information
     * @return
     **/
    private function loadTranslator()
    {
        if ($this->openTranslator() === false) {
            return;
        }

        $this->addExtension(
            new TranslationExtension(
                $this->translator->interface->getTranslator()
            )
        );
    }

    /**
     * Opens a connection to the translation service
     * @return Bool - True if connection succeeded
     **/
    private function openTranslator() : Bool
    {
        $application = $this->getApplication();

        $locale = $this->getServiceConfiguration('locale', true);

        // If no locale has been supplied we do not need to load the translator
        if (is_null($locale)) {
            return false;
        }

        $translator = $application->service->start(
            Service::SERVICE_TRANSLATOR,
            [
                'locale'    =>  $locale

            ]
        );

        $this->setTranslator($translator);

        return true;
    }

    /**
     * Adds an extension to twig
     * @return
     **/
    private function addExtension($extension)
    {
        $this->twig->addExtension($extension);
    }

    /**
     * Sets the twig environment for use by the system
     * @return
     **/
    private function setTwig(\Twig_Environment $twig)
    {
        $this->twig = &$twig;
    }

    /**
     * Returns the twig environment for use by the system
     * @return Twig_Environment - Twig environment
     **/
    private function getTwig() : \Twig_Environment
    {
        return $this->twig;
    }

    /**
     * Sets the for factory environment for use by the system
     * @return
     **/
    private function setFormBuilder(FormFactoryBuilder $formFactory)
    {
        $this->formFactory = &$formFactory;
    }

    /**
     * Returns the form factory environment for use by the system
     * @return FormFactoryBuilder - Form factory builder
     **/
    private function getFormBuilder()
    {
        return $this->formFactory;
    }

    /**
     * Sets the translator stream for use by the system
     * @return
     **/
    private function setTranslator(Service $translator)
    {
        $this->translator = &$translator;
    }

    /**
     * Returns the twig environment for use by the system
     * @return Twig_Environment - Twig environment
     **/
    private function getTranslator()
    {
        return $this->translator;
    }

    /**
     * Adds standard global items to the template data
     * @param Array $templateData - The template data set so far
     * @return
     **/
    private function addGlobalData(array &$templateData = null)
    {
        if (!is_array($templateData)) {
            $templateData = array();
        }

        $application = $this->getApplication();
        $applicationData = $application->getConfig();

        $templateData['cms'] = (!is_null($applicationData) ?
            $applicationData
            : [
                'version' => time()
            ]
        );

        if (!isset($templateData['cms']['url'])) {
            $templateData['cms']['url'] = UrlGenerator::generate('home', '');
        }
    }

    /**
     * Adds the extensions needed to bridge twig with forms and returns a form builder
     * @return FormFactory
     **/
    public function formBuilder() : FormFactory
    {
        $formBuilder = $this->getFormBuilder();
        if (is_null($formBuilder)) {
            $twig = $this->getTwig();

            $formBuilder = Forms::createFormFactoryBuilder();
            $formEngine = new TwigRendererEngine(
                [
                    'layouts/bootstrap_4_layout.twig',
                    'form_customizations.twig',
                ],
                $twig
            );

            $csrfManager = new CsrfTokenManager(
                new UriSafeTokenGenerator(),
                new NativeSessionTokenStorage()
            );

            $formBuilder->addExtension(new CsrfExtension($csrfManager));

            $twig->addRuntimeLoader(new \Twig_FactoryRuntimeLoader([
                TwigRenderer::class => function () use ($formEngine, $csrfManager) {
                    return new TwigRenderer($formEngine, $csrfManager);
                },
            ]));

            $twig->addExtension(new FormExtension());

            $translator = $this->getTranslator();

            if (!is_null($translator)) {
                $translator->interface->translator->addLoader(
                    'xlf',
                    new XliffFileLoader()
                );
            }

            $validator = Validation::createValidator();
            $formBuilder->addExtension(new ValidatorExtension($validator));
            $this->setFormBuilder($formBuilder);
        }

        return $formBuilder->getFormFactory();
    }

    /**
     * Converts the twig template into a HTML string
     * @param TranslationSettings translationSettings - The settings used to translate the text
     * @param String $templateName - The path of the template from the PATH_TEMPLATES directory,
     * omitting the '.twig' extension
     * @param Array $templateData - Any data we wish to pass to the template
     * @return String - HTML string
     **/
    public function fetch(
        TranslationSettings $translationSettings,
        string $templateName,
        array $templateData = null
    ) : string {

        // Add any data we wish to be there on every template load
        $this->addGlobalData($templateData);

        // Load any translator files needed
        $translator = $this->getTranslator();
        if (!is_null($translator)) {
            $translator->interface->loadLibraries($translationSettings);
        }

        // Get our twig instance
        $twig = $this->getTwig();

        // Return our rendered twig data
        return $twig->render(
            $templateName.'.twig',
            $templateData
        );
    }

    /**
     * Converts the twig template into a HTML string then echos it out to screen
     * @param String $templateName - The path of the template from the PATH_TEMPLATES directory,
     * omitting the '.twig' extension
     * @param Array $templateData - Any data we wish to pass to the template
     * @param TranslationSettings translationSettings - The settings used to translate the text
     **/
    public function render(
        TranslationSettings $translationSettings,
        String $templateName,
        array $templateData = null
    ) {
        echo $this->fetch(
            $translationSettings,
            $templateName,
            $templateData
        );
    }
}
