<?php
/**
 * Berryade URL generator - generates URL's relative to the Berryade location
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Symfony\Component\Routing\Generator\UrlGenerator as SymfonyUrlGenerator;
use \Symfony\Component\Routing\RouteCollection;
use \Symfony\Component\Routing\RequestContext;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\Routing\Route;

class UrlGenerator
{
    /**
     * Generates a URL for use within Berryade
     * @param String $routeName - Name to give the route
     * @param String $routePath - Path of the url to output
     * @return String - Url
     **/
    public static function generate(String $routeName, String $routePath) : String
    {
        $routes = new RouteCollection();
        $routes->add($routeName, new Route($routePath));

        $context = new RequestContext();
        $context->fromRequest(Request::createFromGlobals());

        $generator = new SymfonyUrlGenerator($routes, $context);

        return $generator->generate($routeName);
    }
}
