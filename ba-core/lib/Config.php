<?php
/**
 * Loads a config file and returns the parsed data before caching, validating etc if required
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Resource\Finder;

use \Symfony\Component\Config\ConfigCache;
use \Symfony\Component\Config\Resource\FileResource;
use \Symfony\Component\Config\FileLocator;
use \Symfony\Component\Config\Loader\LoaderResolver;
use \Symfony\Component\Config\Loader\DelegatingLoader;
use \Symfony\Component\Config\Definition\ConfigurationInterface;
use \Symfony\Component\Config\Definition\Processor;
use \Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;

class Config
{
    const LOADER_YAML = 'YamlLoader';

    private $fileName;
    private $filePaths;
    private $fileLoader;
    private $fileValidator;
    private $cacheFile;

    /**
     * Set file paramaters and load the config file
     * @param String $filename - Name of the file to load
     * @param String/Array $filePaths - List of paths to search for the above file
     * @param String $fileLoader - Name of the file loader handler
     * @param Bool $useCache - If we should cache this config
     * @param ConfigurationInterface $validator - Object to perform validation
     * @return
     **/
    public function __construct(
        string $fileName,
        $filePaths,
        string $fileLoader,
        bool $useCache = true,
        ConfigurationInterface $validator = null
    ) {

        $this->setFileName($fileName);
        $this->setFilePaths($filePaths);
        $this->setFileLoader($fileLoader);
        $this->setFileValidator($validator);

        if ($useCache) {
            $this->setCacheFile();
        }

        $this->resetConfigValues();
        $this->loadConfigFile();
    }

    /**
     * Returns the name of the file to load
     * @return String containing file name
     **/
    private function getFileName() : string
    {
        return $this->fileName;
    }

    /**
     * Sets the file name of the file to load
     * @param String $fileName - Name of the file to load
     * @return
     **/
    private function setFileName(string $fileName)
    {
        if (!$fileName) {
            throw new \Berryade\Core\Exception\MissingArgument(
                'File name must be provided'
            );
        }

        $this->fileName = $fileName;
    }

    /**
     * Returns the list of paths to search in
     * @return Array of paths
     **/
    private function getFilePaths() : array
    {
        return $this->filePaths;
    }

    /**
     * Sets the locations to search for the file in
     * @param String $filePaths - List of paths to search for the file
     * @return
     **/
    private function setFilePaths($filePaths)
    {
        if (is_string($filePaths)) {
            if (!$filePaths) {
                throw new \Berryade\Core\Exception\MissingArgument(
                    'File path must be provided'
                );
            }

            $filePaths = array($filePaths);
        }

        if (!is_array($filePaths)) {
            throw new \Berryade\Core\Exception\InvalidArgument(
                sprintf(
                    'File path must be an \'Array\', \'%s\' provided',
                    gettype($filePaths)
                )
            );
        }

        $this->filePaths = $filePaths;
    }

    /**
     * Returns the namespace of the file loader
     * @return String containing namespace of loader
     **/
    private function getFileLoader() : string
    {
        return $this->getLoaderClass($this->fileLoader);
    }

    /**
     * Sets the name of the file loader to use
     * @param String $fileLoader - Name of the file loader handler
     * @return
     **/
    private function setFileLoader(string $fileLoader)
    {
        if (!$fileLoader) {
            throw new \Berryade\Core\Exception\MissingArgument(
                'File loader must be provided'
            );
        }

        if (!class_exists($this->getLoaderClass($fileLoader))) {
            throw new \Berryade\Core\Exception\InvalidLoader(
                sprintf(
                    'Loader \'%s\' is not a valid loader',
                    $fileLoader
                )
            );
        }

        $this->fileLoader = $fileLoader;
    }

    /**
     * Returns the namespace of the file validator or null if we are not validating
     * @return String containing namespace of validator
     **/
    private function getFileValidator()
    {
        return $this->fileValidator;
    }

    /**
     * Sets the name of the file validator
     * @param String $fileLoader - Name of the validator handler or null if we are not validating
     * @return
     **/
    private function setFileValidator(ConfigurationInterface $validator = null)
    {
        $this->fileValidator = $validator;
    }

    /**
     * Generates and stores a name to cache the config into
     * @return
     **/
    private function setCacheFile()
    {
        $validator = (
            is_null($this->getFileValidator())
                ? ''
                : get_class($this->getFileValidator())
        );

        $this->cacheFile = 'config_'.str_replace('.', '_', $this->getFileName()) . '_' .
                md5(
                    serialize($this->getFilePaths()) .
                    $this->getFileLoader() .
                    $validator
                ).'.php';
    }

    /**
     * Returns the name of the file to cache to
     * @return String cache file name or null if we should not cache
     **/
    private function getCacheFile()
    {
        $cacheFile = $this->cacheFile;

        if ($cacheFile) {
            $cacheFile = Finder::path(Finder::PATH_CACHE, true) . 'core/' . $cacheFile;
        }

        return $cacheFile;
    }

    /**
     * Resets the stored configuration values
     * @return
     **/
    private function resetConfigValues()
    {
        $this->configValues = array();
    }

    /**
     * Returns the stored configuration values
     * @return Array of configuration values
     **/
    public function getConfigValues() : array
    {
        return $this->configValues;
    }

    /**
     * Stores configuration values
     * @return
     **/
    private function addConfigValues(array $configValues)
    {
        $this->configValues = array_merge($configValues, $this->configValues);
    }

    /**
     * Gets a configuration value or group from the loaded configuration file
     * @param String/Array either a string or an array containing a path of keys to return
     * @return Mixed value of the paramater
     **/
    public function getValue($arrayKey)
    {
        $configValues = $this->getConfigValues();

        if (is_string($arrayKey)) {
            if (!isset($configValues[$arrayKey])) {
                throw new \Berryade\Core\Exception\OutOfBounds(
                    sprintf(
                        'Invalid configuration entry \'%s\'',
                        $arrayKey
                    )
                );
            }

            return $configValues[$arrayKey];
        }

        $currentConfigValues = &$configValues;
        $entryPath = '';

        foreach ($arrayKey as $key) {
            $entryPath .= ($entryPath ? '->'.$key : $key);

            if (!isset($currentConfigValues[$key])) {
                throw new \Berryade\Core\Exception\OutOfBounds(
                    sprintf(
                        'Invalid configuration entry \'%s\'',
                        $entryPath
                    )
                );
            }

            $currentConfigValues = &$currentConfigValues[$key];
        }

        return $currentConfigValues;
    }

    /**
     * Loads config file and returns an array of configuration paramaters
     * @return
     **/
    private function loadConfigFile()
    {
        try {
            $cacheFile = $this->getCacheFile();
            $configCache = ($cacheFile ? new ConfigCache($cacheFile, false) : false);

            // If we select to bypass cache or if the cache is stale fetch new data
            if ($configCache === false || !$configCache->isFresh()) {
                // Find the config file and load the file processor to process the config file
                $locator = new FileLocator($this->getFilePaths());
                $configFiles = $locator->locate($this->getFileName(), null, false);
                $fileLoader = $this->getFileLoader();
                $loaderResolver = new LoaderResolver(array(new $fileLoader($locator)));
                $delegatingLoader = new DelegatingLoader($loaderResolver);
                
                $resources = array();

                // Add values from each matching config file found, merging the results if needed
                foreach ($configFiles as $configFile) {
                    $this->addConfigValues($delegatingLoader->load($configFile));

                    // Add this file we processed to list of resources for the cache
                    if ($configCache !== false) {
                        $resources[] = new FileResource($configFile);
                    }
                }

                // Validate our data if a validator was supplied
                if (($validator = $this->getFileValidator()) instanceof ConfigurationInterface) {
                    // validate our config
                    $processor = new Processor();
                    $this->configValues = $processor->processConfiguration(
                        $validator,
                        $this->getConfigValues()
                    );

                    if (is_callable([$validator, 'convertData'])) {
                        $this->configValues = $validator->convertData($this->configValues);
                    }
                }

                if ($configCache !== false) {
                    // Write our data to a cache file
                    $configCache->write(
                        serialize($this->getConfigValues()),
                        $resources
                    );
                }
            } else {
                $this->addConfigValues(unserialize(file_get_contents($cacheFile)));
            }
        } catch (FileLocatorFileNotFoundException $exception) {
            throw new \Berryade\Core\Exception\FileNotFound(
                'Config file could not be found',
                null,
                $exception
            );
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Returns the full class name for the loader
     * @param String $loaderName - Name of the file loader handler
     * @return String containing the full class name of the loader
     **/
    private function getLoaderClass(string $loaderName) : string
    {
        return '\\Berryade\\Core\\Config\\Loaders\\'.$loaderName;
    }
}
