<?php
/**
 * Application Routing
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\Modules;
use \Berryade\Core\Render;
use \Berryade\Core\UrlGenerator;

use \Berryade\Resource\Finder;

use \Symfony\Component\Config\FileLocator;
use \Symfony\Component\Routing\Matcher\UrlMatcher;
use \Symfony\Component\Routing\Loader\YamlFileLoader;
use \Symfony\Component\Routing\RequestContext;
use \Symfony\Component\Routing\RouteCollection;

class Routing
{
    private $application;

    /**
     * Instantiates the loading of the routefile and matching the supplied path to find a possible
     * route
     * @param Application $application - The berryade application to pass through
     * @param String $path - The path we wish to match to a route or null for the URI
     * @return
     **/
    public function __construct(Application $application, String $path = null)
    {
        if (!$path) {
            $path = $_SERVER['URI'];
        }

        $path = $this->removeUrlTree($path);

        $this->setApplication($application);

        $routes = $this->loadRoutes();
        $this->matchRoute($routes, $path);
    }

    /**
     * Removes the nested portion of the URL
     * @param String $path - URL path
     * @return String - Cleaned URL
     **/
    private function removeUrlTree(String $path) : String
    {
        return substr($path, strlen(UrlGenerator::generate('home', '')) - 1);
    }

    /**
     * Loads the routing file and passes back a collection of possible routes based on its contents
     * @return Routecollection - A collection of possible routes
     **/
    private function loadRoutes() : RouteCollection
    {
        $locator = new FileLocator([ Finder::path(Finder::PATH_APP) ]);
        $loader = new YamlFileLoader($locator);
        $routes = $loader->load('routes.yaml');

        Modules::list($this->getApplication(), true);
        //TODO add module routes
        //TODO add route caching



/*
$rootCollection = new RouteCollection();

$subCollection = new RouteCollection();
$subCollection->add(...);
$subCollection->add(...);
$subCollection->addPrefix('/prefix');
$subCollection->addDefaults(array(...));
$subCollection->addRequirements(array(...));
$subCollection->addOptions(array(...));
$subCollection->setHost('admin.example.com');
$subCollection->setMethods(array('POST'));
$subCollection->setSchemes(array('https'));

$rootCollection->addCollection($subCollection)*/

        return $routes;
    }

    /**
     * Attempts to find a possible route in the supplied collection for the supplied path, then
     * loads the relevant controller responsible for that route
     * @param RouteCollection $routes - A collection of possible routes
     * @param String $path - The path we wish to find a route for
     * @return
     **/
    private function matchRoute(RouteCollection $routes, String $path)
    {
        $context = new RequestContext('/');
        $matcher = new UrlMatcher($routes, $context);

        try {
            $parameters = $matcher->match($path);

            if (strpos($parameters['_controller'], '::') === false) {
                if (!is_callable($parameters['_controller'])) {
                    throw new \Berryade\Core\Exception\LogicException(
                        Translator::translate(
                            'routing.controller.error',
                            [ '%controller%' => $parameters['_controller'] ],
                            new TranslationSettings(),
                            $this->getApplication()
                        )
                    );
                }

                call_user_func($parameters['_controller'], $parameters, $this->application);
            } else {
                list($class, $method) = explode('::', $parameters['_controller'], 2);

                $class = '\\Berryade\\Core\\Controller\\'.$class;

                if (!is_callable(array($class, $method))) {
                    throw new \Berryade\Core\Exception\LogicException(
                        Translator::translate(
                            'routing.class.controller.error',
                            [
                                '%class%' => $class,
                                '%method%' => $method
                            ],
                            new TranslationSettings(),
                            $this->getApplication()
                        )
                    );
                }

                $controller = new $class($this->getApplication(), $parameters);
                call_user_func([$controller, $method]);
            }
        } catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
            $this->routeNotFoundAction($path);
        }
    }

    /**
     * Action to take if the URL does not match any of the routes
     * @param String $path - Requested path
     * @return
     **/
    private function routeNotFoundAction(String $path)
    {
        $application = $this->getApplication();

        header("HTTP/1.0 404 Not Found");
        $render = new Render($application);
        $render->view(
            'error/404',
            [
                'path' => $path
            ],
            new TranslationSettings(
                $application->getConfig('locale')
            )
        );
        exit();
    }

    /**
     * Sets the application for use
     * @param Application $application
     * @return
     **/
    private function setApplication(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Returns the application for use
     * @return Application
     **/
    private function getApplication() : Application
    {
        return $this->application;
    }
}
