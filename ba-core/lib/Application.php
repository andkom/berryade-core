<?php
/**
 * Stores the configuration and manages persistant data such as streams
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Config;
use \Berryade\Core\Services;

class Application
{
    // The version of this application, used for updates or specifying a minimum version
    private $version = '3.0.0';

    // Holds the configuration settings for the application
    private $configuration;

    // Container for the services
    public $service;

    /**
     * Save the configuration settings on instantiation
     * @param \Berryade\Core\Config $config - a configuration object
     * @return
     **/
    public function __construct(ConfigInterface $config = null)
    {
        $this->configuration = $config;

        // Setup our error handling
        new \Berryade\Core\Errors($this);

        // Start our stream handler
        $this->service = new Services($this);
    }

    /**
     * Get the configuration value from the config file, either a direct value or a set of values.
     * @param Mixed $key - String of a direct value key or an Array containing the path to
     * a key
     * @param Bool $suppressError - If we should return an exception rather than throwing one on an
     * invalid key
     * @return Mixed - The value of the configuration item, an array of values for a set or an
     * exception if $supressError = true and the key does not exist.
     **/
    public function getConfig($key = null, Bool $supressError = false)
    {
        if (is_null($this->configuration)) {
            return null;
        }

        if (is_null($key)) {
            return $this->configuration->getConfigValues();
        }

        try {
            $value = $this->configuration->getValue($key);
        } catch (\Exception $e) {
            if ($supressError) {
                return $e;
            }

            throw $e;
        }

        return $value;
    }
}
