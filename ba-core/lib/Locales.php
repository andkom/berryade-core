<?php
/**
 * Interface to allow the system to declare what locales we support
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Locale;

class Locales
{
    const LOCALE_NS = '\\Berryade\\Core\\Locales\\';

    /**
     * Checks to see if a locale is supported in the system
     * @param String locale - the locale we wish to check
     * @return Bool - If we support this locale or not
     **/
    public function supported(String $locale) : Bool
    {
        try {
            $this->getNamespace($locale);
        } catch (\Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * Returns the name space for the local handler
     * @param String locale - the locale we wish to get the namespace for
     * @return String - the namespace
     **/
    private function getNamespace(String $locale) : String
    {
        $locale = strtolower($locale);

        switch ($locale) {
            case 'en_gb':
                return self::LOCALE_NS . 'EnGb';
        }

        throw new \Berryade\Core\Exception\OutOfBounds(
            'Unsupported locale'
        );
    }

    /**
     * Loads and returns the correct locale handler for the supplied localte
     * @param String $locale - Name of the locale to load
     * @return Locale - A locale handler
     **/
    public function load(String $locale) : Locale
    {
        $namespace = $this->getNamespace($locale);
        return new $namespace($locale);
    }
}
