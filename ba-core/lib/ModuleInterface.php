<?php
/**
 * TODO
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

interface ModuleInterface
{
    public function getName() : string;
    public function getDescription() : string;
    public function getUrl() : string;
    public function getVersion() : string;
    public function getVendorName() : string;
    public function getVendorUrl() : string;
    public function getMinPlatformVersion() : string;
    public function getTestedPlatformVersion() : string;
    public function getDependencies() : array;
    public function register() : array;
    public function deregister() : array;
}
