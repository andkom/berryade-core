<?php
/**
 * TOOD
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Resource\Finder;

class ModuleInfo
{
    // The Berryade application
    private $application;
    
    // The location of the module files
    private $path;

    // Whether the module is installed or not
    private $installed;

    // The name of the module as it is installed
    private $identifier;

    // The friendly name of the module
    private $name;

    // The URL of the module support page if applicable
    private $url;

    // The current version of the module
    private $version;

    // The name of the vendor supporting this module
    private $vendorName;

    // The URL to the website of the vendor supporting this module
    private $vendorUrl;

    // The minimum berryade version that must be installed in order to use this module
    private $berryadeMinVersion;

    // The latest version of berryade this module has been tested to support
    private $berryadeTestedVersion;

    // The description of this module
    private $description;


    //TODO
    public function __construct(Application $application, String $path)
    {
        $this->setApplication($application);
        $this->setPath($path);
        $this->setIdentifier(substr($path, strrpos($path, '/') + 1));
        $this->loadData($path);
    }

    //TODO
    private function setApplication(Application $application)
    {
        $this->application = $application;
    }

    //TODO
    private function getApplication() : Application
    {
        return $this->application;
    }

    // TODO
    private function setIsInstalled(bool $installed)
    {
        $this->installed = $installed;
    }

    //TODO
    public function getIsInstalled() : bool
    {
        return $this->installed;
    }

    //TODO
    private function setPath(string $path)
    {
        $this->path = $path;
    }

    //TODO
    public function getPath() : string
    {
        return $this->path;
    }

    //TODO
    private function setUrl(string $url)
    {
        $this->url = $url;
    }

    //TODO
    public function getUrl() : string
    {
        return $this->url;
    }

    //TODO
    private function setDescription(string $description)
    {
        $this->description = $description;
    }

    //TODO
    public function getDescription() : string
    {
        return $this->description;
    }

    //TODO
    private function setMinPlatformVersion(string $minVersion)
    {
        $this->berryadeMinVersion = $minVersion;
    }

    //TODO
    public function getMinPlatformVersion() : string
    {
        return $this->berryadeMinVersion;
    }

    //TODO
    private function setTestedPlatformVersion(string $testedVersion)
    {
        $this->berryadeTestedVersion = $testedVersion;
    }

    //TODO
    public function getTestedPlatformVersion() : string
    {
        return $this->berryadeTestedVersion;
    }

    //TODO
    private function setVersion(string $version)
    {
        $this->version = $version;
    }

    //TODO
    public function getVersion() : string
    {
        return $this->version;
    }

    //TODO
    private function setName(string $name)
    {
        $this->name = $name;
    }

    //TODO
    public function getName() : string
    {
        return $this->name;
    }

    //TODO
    private function setVendorName(string $vendorName)
    {
        $this->vendorName = $vendorName;
    }

    //TODO
    public function getVendorName() : string
    {
        return $this->vendorName;
    }

    //TODO
    private function setVendorUrl(string $vendorUrl)
    {
        $this->vendorUrl = $vendorUrl;
    }

    //TODO
    public function getVendorUrl() : string
    {
        return $this->vendorUrl;
    }

    //TODO
    private function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;
    }

    //TODO
    public function getIdentifier() : string
    {
        return $this->identifier;
    }

    //TODO
    private function loadData(string $path)
    {
        // TODO load data from database fallback to file
        $this->loadFileData($path);
    }

    //TODO
    private function loadFileData(string $path)
    {
        if (!file_exists($path)) {
            return $this->moduleError('Module does not exist');
        }

        if (!file_exists($path . '/Controller.php')) {
            return $this->moduleError('Module missing controller');
        }

        $className = '\\Berryade\\Module\\'.$this->getIdentifier().'\\Controller';
        $moduleInfo = new $className();

        $this->setName($moduleInfo->getName());
        $this->setDescription($moduleInfo->getDescription());
        $this->setUrl($moduleInfo->getUrl());
        $this->setVersion($moduleInfo->getVersion());
        $this->setVendorName($moduleInfo->getVendorName());
        $this->setVendorUrl($moduleInfo->getVendorUrl());
        $this->setMinPlatformVersion($moduleInfo->getMinPlatformVersion());
        $this->setTestedPlatformVersion($moduleInfo->getTestedPlatformVersion());
    }

    //TODO
    private function moduleError(string $error)
    {
        // TODO translate
        throw new \Berryade\Core\Exception\RunTime(
            $error
        );
    }
}
