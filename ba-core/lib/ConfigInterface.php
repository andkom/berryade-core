<?php

namespace Berryade\Core;

interface ConfigInterface
{
    public function getConfigValues() : array;
    public function getValue($arrayKey);
}
