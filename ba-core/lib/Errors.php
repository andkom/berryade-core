<?php
/**
 * Sets up the error paramaters for use during the application
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core;
use \Berryade\Core\Application;

class Errors
{
    private $application;

    /**
     * Set up the error environment for the application
     **/
    public function __construct(Application &$application)
    {
        $this->application = $application;
        $this->setErrorMode();
        $this->setErrorHandler();
    }

    /**
     * Sets the display of error messages
     * @return
     **/
    private function setErrorMode()
    {
        error_reporting(E_ALL);
        
        // Only show errors to screen in test mode
        ini_set('display_errors', $this->application->getConfig(['test']));
    }

    /**
     * Registers an error handler to bubble the error into an exception
     * @return
     **/
    private function setErrorHandler()
    {
        set_error_handler(array($this, 'errorHandler'));
    }

    /**
     * Turn an error into an exception
     * @param Int $errNo - Error Number
     * @param String $errMsg - Error Message
     * @param String $errFile - File error originated from
     * @param Int $errLine - Line error originated from
     * @return
     **/
    public function errorHandler(
        Int $errNo,
        String $errMsg,
        String $errFile = null,
        Int $errLine = null
    ) {

        if ($errFile) {
            $errMsg .= ' in ' . $errFile;
        }

        if ($errLine) {
            $errMsg .= ' on line ' . $errLine;
        }

        throw new \Berryade\Core\Exception\Error(
            $errMsg,
            $errNo
        );
    }
}
