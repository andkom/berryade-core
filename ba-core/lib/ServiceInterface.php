<?php
/**
 * Interface for services to ensure some commonality between them and so they inherit a common type
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

interface ServiceInterface
{
    public function start();
    public function pause();
    public function resume();
    public function end();
}
