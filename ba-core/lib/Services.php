<?php
/**
 * Provides a controller to be able to manage service channels
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;
use \Berryade\Core\Service;

class Services
{
    // Instance of the Berryade application
    private $application;

    /**
     * Stores the berryade application so our services can interact with it
     * @return
     **/
    public function __construct(Application &$application)
    {
        $this->setApplication($application);
    }

    // Conainter of the loaded services
    private $services = [];

    /**
     * Starts a service session, recycling an existing if possible
     * @param String $serviceType - Type of service as listed in \Berryade\Core\Service::SERVICE_X
     * @param Array $configurationParams - Paramaters to pass through to the service
     * @return Service
     **/
    public function &start(String $serviceType, array $configurationParams = null) : Service
    {
        $service = new Service($this, $serviceType, $configurationParams);
        $this->launchService($service);

        return $service;
    }

    /**
     * Ends a service session, marking it available for reuse
     * @param Service $service - The service you wish to end
     * @return
     **/
    public function end(Service $service)
    {
        $channel = $service->getChannel();
        $serviceId = $service->getServiceId();

        if (!isset($this->services[$serviceId]['used'][$channel])) {
            return;
        }

        unset($this->services[$serviceId]['used'][$channel]);

        if (!isset($this->services[$serviceId]['all'][$channel])) {
            return;
        }

        $this->services[$serviceId]['ready'][$channel] = true;
        $this->services[$serviceId]['all'][$channel]->interface->pause();
    }

    /**
     * Destroys a service session
     * @param Service $service - The service you wish to destroy
     * @return
     **/
    public function destroy(Service $service)
    {
        $channel = $service->getChannel();
        $serviceId = $service->getServiceId();

        if (isset($this->services[$serviceId]['used'][$channel])) {
            unset($this->services[$serviceId]['used'][$channel]);
        }

        if (isset($this->services[$serviceId]['ready'][$channel])) {
            unset($this->services[$serviceId]['ready'][$channel]);
        }

        if (!isset($this->services[$serviceId]['all'][$channel])) {
            return;
        }

        $this->services[$serviceId]['all'][$channel]->interface->end();

        unset($this->services[$serviceId]['all'][$channel]);
    }

    /**
     * Takes a basic uninstantiated service and replaces it with a matching recycled one if
     * available or instantiates it if not
     * @param Service $service - The basic service you wish to instantiate
     * @return
     **/
    private function launchService(Service &$service)
    {
        $serviceId = $service->getServiceId();

        if (!isset($this->services[$serviceId]['ready']) ||
            !count($this->services[$serviceId]['ready'])) {
            return $this->instantiateService($service);
        }

        // Get channel
        end($this->services[$serviceId]['ready']);
        $channel = key($this->services[$serviceId]['ready']);

        // Move from being ready to being used
        unset($this->services[$serviceId]['ready'][$channel]);
        $this->services[$serviceId]['used'][$channel] = true;
        $service = $this->services[$serviceId]['all'][$channel];

        $this->services[$serviceId]['all'][$channel]->interface->resume();
    }

    /**
     * Instantiates a basic service
     * @param Service $service - The service you wish to instantiate
     * @return
     **/
    private function instantiateService(Service &$service)
    {
        $serviceId = $service->getServiceId();
        
        if (!isset($this->services[$serviceId])) {
            $this->services[$serviceId] = $this->newServiceContainer();
        }

        // Create a new service interface
        $this->services[$serviceId]['all'][] = &$service;

        // Assign our channel
        end($this->services[$serviceId]['all']);
        $channel = key($this->services[$serviceId]['all']);

        $service->setChannel($channel);

        // Mark this channel as in use
        $this->services[$serviceId]['used'][$channel] = true;

        // Send the start signal
        $service->interface->start();
    }

    /**
     * Template for a service tracker container
     * @return Array - service tracker container
     **/
    private function newServiceContainer() : Array
    {
        return ['used' => [], 'ready' => [], 'all' => []];
    }

    /**
     * Stores the application for use by the services
     * @return
     **/
    private function setApplication(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Returns the application for use by the services
     **/
    public function getApplication() : Application
    {
        return $this->application;
    }
}
