<?php
/**
 * Actions to perform for each controller and so controllers can be grouped
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Core\Application;

class Controller
{
    // Berryade application
    protected $application;
    // Paramaters to pass to the controller
    private $paramaters;

    /**
     * Save a link to the application and save route paramaters for use by the controller
     * @param Application $application - Berryade application
     * @param Array $paramaters - route paramaters
     * @return
     **/
    public function __construct(Application $application, array $paramaters)
    {
        $this->setApplication($application);
        $this->setParamaters($paramaters);
    }

    /**
     * Sets the application for use by the controller
     * Application $application - Berryade application
     * @return
     **/
    private function setApplication(Application &$application)
    {
        $this->application = $application;
    }

    /**
     * Returns the application for use by the controller
     * @return Application $application - Berryade application
     **/
    protected function getApplication() : Application
    {
        return $this->application;
    }

    /**
     * Sets the paramaters for use by the controller
     * Array $paramaters - Route paramaters
     * @return
     **/
    private function setParamaters(array $paramaters)
    {
        $this->paramaters = $paramaters;
    }
}
