<?php
/**
 * Common Data functions
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

class Data
{
    /**
     * Returns data nested within an array, or the whole arrayif no key is supplied
     * @param array $data - Data container
     * @param string/array/null $key - Key to return data for, this could be nested in an array for
     * a deeper path, a string for bottom level or null for the whole array
     * @param Bool suppressError - If true then return null if the item is not set instead of
     * throwing an out of bounds error
     * @param Application application
     * @return mixed - the value of the item
     *
     **/
    public static function getNestedValue(
        Application $application,
        array $data,
        $key = null,
        bool $supressError = false
    ) {
        if (is_null($data)) {
            return null;
        }

        // Return all values if no key is set
        if (is_null($key)) {
            return $data;
        }

        if (!is_array($key)) {
            $key = array($key);
        }

        $lastItem = $data;
        $path = '';

        try {
            foreach ($key as $itemKey) {
                $path .= $itemKey.'->';

                if (!isset($lastItem[$itemKey])) {
                    throw new \Berryade\Core\Exception\OutOfBounds(
                        \Berryade\Core\Translator::translate(
                            'key.does.not.exist',
                            [
                                '%key%' => substr($path, 0, -2)
                            ],
                            new TranslationSettings($application->getConfig('locale')),
                            $application
                        )
                    );
                }

                $lastItem = $lastItem[$itemKey];
            }
        } catch (\Exception $exception) {
            if ($supressError) {
                return null;
            }

            throw $exception;
        }

        return $lastItem;
    }
}
