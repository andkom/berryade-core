<?php
/**
 * TODO
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core;

use \Berryade\Resource\Finder;

class Register
{
    // TODO
    public static function registerUnattended()
    {
        $application = require Finder::path(Finder::PATH_APP) . 'ba-core/bootstrap.php';

        if (!($application instanceof \Berryade\Core\Application)) {
            die('Cannot load the Berryade framework: ' . $application);
        }

        self::clearCache();
        self::registerCore($application);
        self::registerModules($application);
    }

    // TODO
    public static function registerCore(Application $application)
    {
        echo '    Registering Berryade Core... ';
        echo 'done' . PHP_EOL;
    }

    // TODO
    public static function clearCache()
    {
        echo '    Clear Cache... ';

        $fileCount = array('success' => 0, 'all' => 0);

        $directoryIterator = new \RecursiveDirectoryIterator(
            Finder::path(Finder::PATH_CACHE),
            \RecursiveDirectoryIterator::SKIP_DOTS
        );

        $files = new \RecursiveIteratorIterator(
            $directoryIterator,
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $file) {
            if (!$file->isDir()) {
                $fileCount['all']++;
               
                if (@unlink($file->getRealPath()) === true) {
                    $fileCount['success']++;
                }
            }
        }

        echo 'deleted (' . $fileCount['success'] . '/' . $fileCount['all'] . ')' . PHP_EOL;
    }

    // TODO
    private static function registerModules(Application $application)
    {
        echo '    Registering Modules... ' . PHP_EOL;

        $modules = Modules::List($application, null);

        $config = $application->getConfig([ 'mysql', 'default' ], false);
        var_dump($config);

        foreach ($modules as $module) {
            // IS in db
            // no insert
            // yes then perform update then run the module registration script

            // TODO register
            echo '        ' . $module->getName() . ' (' . $module->getIdentifier() . ')... ';

            echo 'done' . PHP_EOL;
        }
    }

    private static function updateModule(Application $application, ModuleInfo $module)
    {
       /* $translator = $application->service->start(
            Service::SERVICE_MYSQL,
            [
                'locale' => $translationSettings->getLocale()
            ]
        );

        $translation = $translator->interface->translate($key, $data, $translationSettings);

        $application->service->end($translator);
        return $translation;*/
    }
}
