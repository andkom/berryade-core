<?php
/**
 * Controller for the setup route
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Controller;

use \Berryade\Core\Form\SetupHolding as HoldingForm;
use \Berryade\Core\Form\SetupWizard as WizardForm;

use \Berryade\Core\Render;
use \Berryade\Core\Translator;
use \Berryade\Core\TranslationSettings;
use \Berryade\Core\UrlGenerator;

use \Berryade\Resource\Finder;

use \Symfony\Component\Yaml\Yaml;

class Setup extends \Berryade\Core\Controller
{
    /**
     * Action to take when user requests the setup url, if the berryade.yaml does not exist, else
     * show a permission denied message
     * @return
     **/
    public function setupDefaultAction()
    {
        $this->configExistsAction();
        $this->setupHoldingAction();
    }

    /**
     * Action to take if the user is trying to access the setup actions when the configuration file
     * is already present
     * @return
     **/
    private function configExistsAction()
    {
        if (!$this->configExists()) {
            return;
        }

        $application = $this->getApplication();
        $translationSettings = new TranslationSettings($application->getConfig('locale'));
        $renderer = new Render($application);

        $renderer->view(
            'error/fatal',
            [   'message' =>
                Translator::translate(
                    'site.configuration.ran',
                    null,
                    $translationSettings,
                    $application
                )
            ],
            $translationSettings
        );
        exit();
    }

   /**
     * Action to take when user navigates to /setup/wizard/ or has opted to use the wizard to
     * construct a configuration file
     * @return
     **/
    public function setupWizardAction()
    {
        $this->configExistsAction();
        $application = $this->getApplication();

        $translationSettings = new TranslationSettings(
            $application->getConfig('locale')
        );

        $renderer = new Render($this->application);

        $renderer->startService($translationSettings);

        $formBuilder = $renderer->formBuilder($translationSettings);

        $form = $formBuilder
            ->createBuilder(
                WizardForm::class,
                null,
                [
                    'method' => 'post'
                ]
            )
            ->getForm()
        ;

        // Divert people back to the holding action if needed
        $form->handleRequest();
        if ($form->isSubmitted()) {
            if ($form->get('cancel')->isClicked()) {
                header('Location: '.UrlGenerator::generate('setup', 'setup/', true, 302));
                exit();
            }

            if ($form->isValid()) {
                $config = $form->getData();

                file_put_contents(
                    Finder::path(Finder::PATH_ROOT) . 'berryade.yaml',
                    Yaml::dump([ 'berryade' => $config])
                );

                header('Location: ' . UrlGenerator::generate('home', '', true, 302));
                exit();
            }
        }

        $renderer->view(
            'application/setup/wizard',
            [
                'form'   => $form->createView()
            ],
            $translationSettings
        );

        exit();
    }

    /**
     * Action to take when user navigates to /setup/
     * @return
     **/
    private function setupHoldingAction()
    {
        $application = $this->getApplication();

        $translationSettings = new TranslationSettings(
            $application->getConfig('locale')
        );

        $renderer = new Render($this->application);

        $renderer->startService($translationSettings);

        $formBuilder = $renderer->formBuilder($translationSettings);

        $form = $formBuilder
            ->createBuilder(
                HoldingForm::class,
                null,
                [
                    'method' => 'post'
                ]
            )
            ->getForm()
        ;

        // Divert people to the wizard action if needed
        $form->handleRequest();
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('wizard')->isClicked()) {
                header('Location: '.UrlGenerator::generate('setup', 'setup/wizard/', true, 302));
                exit();
            }
        }

        $renderer->view(
            'application/setup/index',
            [
                'form'   => $form->createView()
            ],
            $translationSettings
        );

        exit();
    }

    /**
     * Check to see if the configuration file already exists
     * @return Bool
     **/
    private function configExists() : Bool
    {
        return file_exists(
            Finder::path(Finder::PATH_ROOT) .
            'berryade.yaml'
        );
    }
}
