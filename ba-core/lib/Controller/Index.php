<?php
/**
 * Controller for the Index route
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Controller;

use \Berryade\Core\Render;
use \Berryade\Core\TranslationSettings;

class Index extends \Berryade\Core\Controller
{
    /**
     * Default page for the application
     * @return
     **/
    public function dashboard()
    {
        $application = $this->getApplication();

        $translationSettings = new TranslationSettings(
            $application->getConfig('locale')
        );

        $renderer = new Render($this->application);

        $renderer->startService($translationSettings);

        $renderer->view(
            'application/index',
            null,
            $translationSettings
        );
        exit();
    }
}
