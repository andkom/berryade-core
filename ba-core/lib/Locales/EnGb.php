<?php
/**
 * Locale file for England - Great Britain
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Core\Locales;

class EnGb extends \Berryade\Core\Locale
{
}
