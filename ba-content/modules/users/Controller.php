<?php
/**
 * TODO
 *
 * @package    Berryade
 * @copyright  2007 - 2017 Inkberry Creative Ltd - All Rights Reserved
 * @license    Proprietary and confidential
 * @version    1.0.0
 * @since      File available since Berryade 3.0.0
 **/

namespace Berryade\Module\users;

class Controller implements \Berryade\Core\ModuleInterface
{
    // TODO translate?
    private $name = 'Users';
    private $url = '';
    private $version = '1.0.0';
    private $vendorName  = 'Inkberry Creative Ltd';
    private $vendorUrl  = 'https://www.inkberrycreative.co.uk/';
    private $berryadeTestedVersion = '3.0.0';
    private $berryadeMinVersion = '3.0.0';
    // TODO translate?
    private $description = 'Manage user accounts within the Berryade system';

    public function getVersion() : string
    {
        return $this->version;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getUrl() :string
    {
        return $this->url;
    }

    public function getVendorName() : string
    {
        return $this->vendorName;
    }

    public function getVendorUrl() : string
    {
        return $this->vendorUrl;
    }

    public function getMinPlatformVersion() : string
    {
        return $this->berryadeMinVersion;
    }

    public function getTestedPlatformVersion() : string
    {
        return $this->berryadeTestedVersion;
    }

    public function getDependencies() : array
    {
        return [];
    }

    public function register() : array
    {
        return [];
    }

    public function deregister() : array
    {
        return [];
    }
}
